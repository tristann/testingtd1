# testingTD1
## Description
PW made with JavaScript and JEST
## Installation
In order to install and run the project well, you have to run 'npm i'
## Usage
In order to run tests, you have to run 'npm run test'
## Link
[TP](https://github.com/dmerejkowsky/kata-rpn)
