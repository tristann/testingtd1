const { rpnCalculator, rpnCalculatorGoingFurther } = require('../helpers');

describe("[RPN_TEST]", () => {

  // EXPECTING RESULTS
  test("RPN_Calulator should return the result of the addition between 4 & 5 by returning a single int value", () => {
    const res = rpnCalculator("4 5 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(9);
  });

  test("RPN_Calulator should return the result of the substraction between 4 & 5 by returning a single int value", () => {
    const res = rpnCalculator("5 4 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(1);
  });


  test("RPN_Calulator should return the result of the substraction between 7 & 5 by returning a single negative int value", () => {
    const res = rpnCalculator("5 7 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(-2);
  });

  test("RPN_Calulator should return the initial value in the case it is the single one", () => {
    const res = rpnCalculator("4");

    expect(res.code).toEqual("02");
  });

  test('RPN_Calulator should return the result of the consecutive addition of 1, 2 and 3', () => {
    const res = rpnCalculator("1 2 + 3 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(6);
  });

  test('RPN_Calulator should return the result of the addition of 1, 2 and substraction of the result with 3', () => {
    const res = rpnCalculator("1 2 + 3 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(0);
  });

  test('RPN_Calulator should return the result of the addition of -1 and 2 which is 1', () => {
    const res = rpnCalculator("-1 2 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(1);
  });

  test('RPN_Calulator should return the result of the substraction of -1 and 2 which is -3', () => {
    const res = rpnCalculator("-1 2 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(-3);
  });

  test('RPN_Calulator should return the same entry if only one value is sent with an operator', () => {
    const res = rpnCalculator("10 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(10);
  });

  test('RPN_Calulator should return the same entry if only one value is sent with an operator', () => {
    const res = rpnCalculator("10 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(-10);
  });

  test('RPN_Calulator should return the same entry if only one value is sent with an operator several times', () => {
    const res = rpnCalculator("10 - 2 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(-8);
  });

  test('RPN_Calulator should return the result of the division between the same two big numbers', () => {
    const res = rpnCalculator("900719925474099267900719925474099267900719925474099267 900719925474099267900719925474099267900719925474099267 /");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(1);
  });

  // EXPECTING ERRORS

  test("RPN_Calulator should return an error with a specific code if no operator sent with 2 values", () => {
    const res = rpnCalculator("4 5");

    expect(res.code).toEqual("02");
  });

  test("RPN_Calulator should return an error with a specific code if just one operator is sent", () => {
    const res = rpnCalculator("+");

    expect(res.code).toEqual("02");
  });

  test("RPN_Calulator should return an error with a specific code if wrong input order is sent", () => {
    const res = rpnCalculator("+ 5 4");

    expect(res.code).toEqual("02");
  });

  test("RPN_Calulator should return an error with a specific code if there is too much spaces between values", () => {

    const resBeforeOperator = rpnCalculator("4 5  +");
    const resBeforeValue = rpnCalculator("4  6 -");

    expect(resBeforeOperator.code).toEqual("02");
    expect(resBeforeValue.code).toEqual("02");
  });

  test("RPN_Calulator should return an error with a specific code if no operator sent with 1 value", () => {
    const res = rpnCalculator("4 +");

    expect(res.code).toEqual("02");
  });

  test('RPN_Calulator should return an error with a specific code if trying to divide by 0', () => {
    const res = rpnCalculator("4 0 /");

    expect(res.code).toEqual("03");
  });

  test('RPN_Calulator should return an error with a specific code if trying to divide 0 by 0', () => {
    const res = rpnCalculator("0 0 /");

    expect(res.code).toEqual("03");
  });

  test('RPN_Calulator should return an error with a specific code if trying to divide by 0', () => {
    const res = rpnCalculator("4 0000 /");

    expect(res.code).toEqual("03");
  });

  test('RPN_Calulator should return an error with a specific code if 2 power 2 is sent', () => {
    const res = rpnCalculator("4 2e2 +");

    expect(res.code).toEqual("02");
  });

  test('RPN_Calulator should return an error with a specific code if FXX123 is sent', () => {
    const res = rpnCalculator("4 FXX123 +");

    expect(res.code).toEqual("02");
  });

  test('RPN_Calulator should return an error with a specific code if 0xF is sent', () => {
    const res = rpnCalculator("4 0xF +");

    expect(res.code).toEqual("02");
  });

  test('RPN_Calulator should return an error with a specific code if empty string sent', () => {
    const res = rpnCalculator("");

    expect(res.code).toEqual("02");
  });

  test('RPN_Calulator should return an error with a specific code if Array sent', () => {
    const res = rpnCalculator([]);

    expect(res.code).toEqual("01");
  });
  test('RPN_Calulator should return an error with a specific code if null sent', () => {
    const res = rpnCalculator(null);

    expect(res.code).toEqual("01");
  });


  test('RPN_Calulator should return an error with a specific code if a function is sent in a string', () => {
    const res = rpnCalculator("console.log('coucou je suis un pirate')");

    expect(res.code).toEqual("02");
  });

  test('RPN_Calulator should return an error with a specific code if nothing is sent', () => {
    const res = rpnCalculator();

    expect(res.code).toEqual("01");
  });


  test('RPN_Calulator should return an error with a specific code if not all values are not of a specific type', () => {
    const res = rpnCalculator("4 blabla +");

    expect(res.code).toEqual("02");
  });

  test('RPN_Calulator should return an error with a specific code if a single operator is sent', () => {
    const resAddition = rpnCalculator("+");
    const resSubstraction = rpnCalculator("-");

    expect(resAddition.code).toEqual("02");
    expect(resSubstraction.code).toEqual("02");
  });


  test('RPN_Calulator should return an error with a specific code if a special character is sent', () => {
    const res = rpnCalculator("3 ' -");

    expect(res.code).toEqual("02");
  });

  test('RPN_Calulator should return an error with a specific code if wrong argument number is sent', () => {
    const res = rpnCalculator("3 ' -", "3");

    expect(res.code).toEqual("02");
  });

  test('RPN_Calulator should return an error with a specific code if two operators are sent but with missing operator', () => {
    const res = rpnCalculator("1 2 + 4");

    expect(res.code).toEqual("02");
  });
});

describe("[rpnCalculatorGoingFurther_TEST]", () => {

  // EXPECTING RESULTS
  test("rpnCalculatorGoingFurther should return the result of the addition between 4 & 5 by returning a single int value", () => {
    const res = rpnCalculatorGoingFurther("4 5 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(9);
  });

  test("rpnCalculatorGoingFurther should return the result of the substraction between 4 & 5 by returning a single int value", () => {
    const res = rpnCalculatorGoingFurther("5 4 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(1);
  });


  test("rpnCalculatorGoingFurther should return the result of the substraction between 7 & 5 by returning a single negative int value", () => {
    const res = rpnCalculatorGoingFurther("5 7 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(-2);
  });

  test("rpnCalculatorGoingFurther should return the initial value in the case it is the single one", () => {
    const res = rpnCalculatorGoingFurther("4");

    expect(res.code).toEqual("02");
  });

  test('rpnCalculatorGoingFurther should return the result of the consecutive addition of 1, 2 and 3', () => {
    const res = rpnCalculatorGoingFurther("1 2 + 3 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(6);
  });

  test('rpnCalculatorGoingFurther should return the result of the addition of 1, 2 and substraction of the result with 3', () => {
    const res = rpnCalculatorGoingFurther("1 2 + 3 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(0);
  });

  test('rpnCalculatorGoingFurther should return the result of the addition of -1 and 2 which is 1', () => {
    const res = rpnCalculatorGoingFurther("-1 2 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(1);
  });

  test('rpnCalculatorGoingFurther should return the result of the substraction of -1 and 2 which is -3', () => {
    const res = rpnCalculatorGoingFurther("-1 2 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(-3);
  });

  test('rpnCalculatorGoingFurther should return the same entry if only one value is sent with an operator', () => {
    const res = rpnCalculatorGoingFurther("10 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(10);
  });

  test('rpnCalculatorGoingFurther should return the same entry if only one value is sent with an operator', () => {
    const res = rpnCalculatorGoingFurther("10 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(-10);
  });

  test('rpnCalculatorGoingFurther should return the same entry if only one value is sent with an operator several times', () => {
    const res = rpnCalculatorGoingFurther("10 - 2 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(-8);
  });

  test('rpnCalculatorGoingFurther should return the result of the substraction between a big number and an other - testing precision of parseInt', () => {
    const res = rpnCalculatorGoingFurther("999999999999999999999999999999999999999999999999999999999999999999999 9 -");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(999999999999999999999999999999999999999999999999999999999999999999990);
  });

  test("rpnCalculatorGoingFurther should return the result of the square root of 9", () => {
    const res = rpnCalculatorGoingFurther("9 sqrt");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(3);
  });

  test("rpnCalculatorGoingFurther should return the result of the square root of 1 which is 1", () => {
    const res = rpnCalculatorGoingFurther("1 sqrt");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(1);
  });

  test("rpnCalculatorGoingFurther should return the maximum of 1 2 3 4 5", () => {
    const res = rpnCalculatorGoingFurther("1 2 3 4 5 max");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(5);
  });

  test("rpnCalculatorGoingFurther should return the maximum of 5 4 3 2 1", () => {
    const res = rpnCalculatorGoingFurther("5 4 3 2 1 max");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(5);
  });

  test("rpnCalculatorGoingFurther should return the maximum of 1 3 5 4 2", () => {
    const res = rpnCalculatorGoingFurther("1 3 5 4 2 max");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(5);
  });

  test("rpnCalculatorGoingFurther should return the maximum of 1 3 -5 4 2", () => {
    const res = rpnCalculatorGoingFurther("1 3 -5 4 2 max");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(4);
  });

  test("rpnCalculatorGoingFurther should return the maximum of -1 -3 -5 -4 -2", () => {
    const res = rpnCalculatorGoingFurther("-1 -3 -5 -4 -2 max");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(-1);
  });

  test("rpnCalculatorGoingFurther should return the maximum of 1 1 1 1", () => {
    const res = rpnCalculatorGoingFurther("1 1 1 1 max");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(1);
  });

  // EXPECTING ERRORS
  test("rpnCalculatorGoingFurther should return an error with a specific code if no operator sent with 2 values", () => {
    const res = rpnCalculatorGoingFurther("4 5");

    expect(res.code).toEqual("02");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if just one operator is sent", () => {
    const res = rpnCalculatorGoingFurther("+");

    expect(res.code).toEqual("03");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if wrong input order is sent", () => {
    const res = rpnCalculatorGoingFurther("+ 5 4");

    expect(res.code).toEqual("02");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if there is too much spaces between values", () => {

    const resBeforeOperator = rpnCalculatorGoingFurther("4 5  +");
    const resBeforeValue = rpnCalculatorGoingFurther("4  6 -");

    expect(resBeforeOperator.code).toEqual("02");
    expect(resBeforeValue.code).toEqual("02");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if no operator sent with 1 value", () => {
    const res = rpnCalculatorGoingFurther("4 +");

    expect(res).not.toBeNull();
    expect(res.length).toBe(1);
    expect(res[0]).toBe(4);
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if trying to divide by 0', () => {
    const res = rpnCalculatorGoingFurther("4 0 /");

    expect(res.code).toEqual("03");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if trying to divide 0 by 0', () => {
    const res = rpnCalculatorGoingFurther("0 0 /");

    expect(res.code).toEqual("03");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if trying to divide by 0', () => {
    const res = rpnCalculatorGoingFurther("4 0000 /");

    expect(res.code).toEqual("03");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if 2 power 2 is sent', () => {
    const res = rpnCalculatorGoingFurther("4 2e2 +");

    expect(res.code).toEqual("02");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if FXX123 is sent', () => {
    const res = rpnCalculatorGoingFurther("4 FXX123 +");

    expect(res.code).toEqual("02");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if 0xF is sent', () => {
    const res = rpnCalculatorGoingFurther("4 0xF +");

    expect(res.code).toEqual("02");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if empty string sent', () => {
    const res = rpnCalculatorGoingFurther("");

    expect(res.code).toEqual("02");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if Array sent', () => {
    const res = rpnCalculatorGoingFurther([]);

    expect(res.code).toEqual("01");
  });
  test('rpnCalculatorGoingFurther should return an error with a specific code if null sent', () => {
    const res = rpnCalculatorGoingFurther(null);

    expect(res.code).toEqual("01");
  });


  test('rpnCalculatorGoingFurther should return an error with a specific code if a function is sent in a string', () => {
    const res = rpnCalculatorGoingFurther("console.log('coucou je suis un pirate')");

    expect(res.code).toEqual("02");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if nothing is sent', () => {
    const res = rpnCalculatorGoingFurther();

    expect(res.code).toEqual("01");
  });


  test('rpnCalculatorGoingFurther should return an error with a specific code if not all values are not of a specific type', () => {
    const res = rpnCalculatorGoingFurther("4 blabla +");

    expect(res.code).toEqual("02");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if a single operator is sent', () => {
    const resAddition = rpnCalculatorGoingFurther("+");
    const resSubstraction = rpnCalculatorGoingFurther("-");

    expect(resAddition.code).toEqual("03");
    expect(resSubstraction.code).toEqual("03");
  });


  test('rpnCalculatorGoingFurther should return an error with a specific code if a special character is sent', () => {
    const res = rpnCalculatorGoingFurther("3 ' -");

    expect(res.code).toEqual("02");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if wrong argument number is sent', () => {
    const res = rpnCalculatorGoingFurther("3 ' -", "3");

    expect(res.code).toEqual("02");
  });

  test('rpnCalculatorGoingFurther should return an error with a specific code if two operators are sent but with missing operator', () => {
    const res = rpnCalculatorGoingFurther("1 2 + 4");

    expect(res.code).toEqual("02");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if wrong string input is sent", () => {
    const res = rpnCalculatorGoingFurther("9 4 sqrt");

    expect(res.code).toEqual("03");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if nothing is sent", () => {
    const res = rpnCalculatorGoingFurther();

    expect(res.code).toEqual("01");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if null is sent", () => {
    const res = rpnCalculatorGoingFurther(null);

    expect(res.code).toEqual("01");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if wrong input is sent", () => {
    const res = rpnCalculatorGoingFurther("1 ");

    expect(res.code).toEqual("02");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if only one operator is sent", () => {
    const res = rpnCalculatorGoingFurther("sqrt");

    expect(res.code).toEqual("03");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if wrong input is sent", () => {
    const res = rpnCalculatorGoingFurther("sqrt sqrt");

    expect(res.code).toEqual("03");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if wrong input order is sent", () => {
    const res = rpnCalculatorGoingFurther("sqrt 9");

    expect(res.code).toEqual("02");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if trying to have square root of negative number", () => {
    const res = rpnCalculatorGoingFurther("-9 sqrt");

    expect(res.code).toEqual("03");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if trying to have square root of 0 ", () => {
    const res = rpnCalculatorGoingFurther("0 sqrt");

    expect(res.code).toEqual("03");
  });

  test("rpnCalculatorGoingFurther should return an error with a specific code if wrong input number is sent", () => {
    const res = rpnCalculatorGoingFurther("9 sqrt 9");

    expect(res.code).toEqual("02");
  });


  test("rpnCalculatorGoingFurther should return an error with a specific code if wrong input number is sent", () => {
    const res = rpnCalculatorGoingFurther("1 5 3 max 9");

    expect(res.code).toEqual("02");
  });
});