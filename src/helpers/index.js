const { DEFAULT } = require('../constants')
var validator = require('validator');

/**
 * @description this function is used to perform basics operation.
 * 
 * @param {string} expreVal represent the string expression for the operations. 
 * @returns {[int] | Error } either an error or the result of the operations.
 */
function rpnCalculator(expreVal) {
    let stackRes = []

    if (typeof expreVal != "string") {
        return {
            msg: `Dear Mr. Alex, the input must be a string not a '${typeof expreVal}''`,
            code: "01"
        }
    }

    const elements = expreVal.split(' ')

    if (!verifyExpression(elements)) {
        return {
            msg: `Dear Mr. Alex, the expression can only contains numerics values and [${DEFAULT.OPERATORS}]`,
            code: "02"
        }
    }

    for (let index = 0; index < elements.length; index++) {
        if (index > 0 && index % 2 == 0) {
            try {
                stackRes = [operations(elements[index], stackRes[0], stackRes[1])]
            } catch (error) {
                return {
                    msg: error.msg,
                    code: "03"
                }
            }

        } else {
            stackRes.push(parseInt(elements[index]))
        }
    }

    return stackRes
}

/**
 * @description This function is used to verify all the sliced elements of the string containning all the operations
 * 
 * @param {[string]} elements contains all the elements needed to perform all the operations
 * @returns {boolean} `True` if all the elements respect the constraint `False` otherwise.
 */
function verifyExpression(elements) {
    let ans = true
    let cpt = 0

    if (!(elements.length % 3 == 0)) {
        return false
    }

    while (ans == true && cpt < elements.length) {
        if (validator.isNumeric(elements[cpt], 'fr-FR') || DEFAULT.OPERATORS.includes(elements[cpt])) {
            ans = true
        } else {
            ans = false
        }
        cpt += 1
    }
    return ans
}

/**
 * @description this function is used to performs all the operation.
 * 
 * @param {string} op represents the string of the operation to perform.
 * @param {string} a represents the first element in the operation.
 * @param {string} b represents the second element in the operation.
 * @returns the results of the operation or an error.
 */
function operations(op, a, b) {
    switch (op) {
        case '-':
            return a - b
        case '+':
            return a + b
        case '/':
            if (b == 0) { throw new Error("Dear Mr. Alex, operation with divide by 0 is not allowed") }
            return a / b
        case '*':
            return a * b
    }
}

//Going further
/**
 * @description this function is used to process the operations for the `Further` section in the readme.
 * 
 * @param {string} expreVal represent the string expression for the operations.
 * @returns {[int] | Error } either an error or the result of the operations.
 */
function rpnCalculatorGoingFurther(expreVal) {
    let stackRes = []
    let operations = []

    if (typeof expreVal != "string") {
        return {
            msg: `Dear Mr. Alex, the input must be a string not a '${typeof expreVal}''`,
            code: "01"
        }
    }

    const elements = expreVal.toLowerCase().split(' ')

    if (!verifyExpressionGoingFurther(elements)) {
        return {
            msg: `Dear Mr. Alex, the expression can only contains numerics values and [${DEFAULT.OPERATORS}]`,
            code: "02"
        }
    }

    operations = createOperations(elements)

    operations.forEach(element => {

        try {
            stackRes = operationsGoingFurther(element, stackRes)
        } catch (error) {
            stackRes = {
                msg: error.message,
                code: "03"
            }
        }
    })

    return stackRes
}


/**
 * @description This function is used to verify all the sliced elements of the string containning all the operations
 * 
 * @param {[string]} elements contains all the elements needed to perform all the operations
 * @returns {boolean} `True` if all the elements respect the constraint `False` otherwise.
 */
function verifyExpressionGoingFurther(elements) {
    let ans = true
    let cpt = 0

    if (!DEFAULT.OPERATORS.includes(elements[elements.length - 1])) { return false }

    cpt = 0
    ans = true

    while (ans == true && cpt < elements.length) {
        if (validator.isNumeric(elements[cpt], 'fr-FR') || DEFAULT.OPERATORS.includes(elements[cpt])) {
            ans = true
        } else {
            ans = false
        }
        cpt += 1
    }
    return ans
}

/**
 * @description this function is used to create a operation struct
 * 
 * @param {[string]} elements contains all the elements needed to perform all the operations
 * @returns an array of struct containning all the operations.
 */
function createOperations(elements) {
    let ans = []
    let tempValues = []

    for (let index = 0; index < elements.length; index++) {
        if (DEFAULT.OPERATORS.includes(elements[index])) {
            ans.push({ values: tempValues, operation: elements[index] })
            tempValues = []
        } else {
            tempValues.push(elements[index])
        }
    }

    return ans
}

/**
 * @description this function is used to performs all the operation.
 * 
 * @param {[operation]} operation represents the operation to perform.
 * @param {[operation]} stackRes represents array where the result of the operation will be stored.
 * @returns the results of the operation or an error.
 */
function operationsGoingFurther(operation, stackRes) {
    switch (operation.operation) {
        case '-':
            if (stackRes.length > 0) {
                operation.values.unshift(stackRes[0])
                stackRes = []
            }

            if(operation.values.length < 1 ){
                throw new Error("Dear Mr. Alex, operation not allowed")
            }

            if (operation.values.length == 1) {
                stackRes.push(0 - parseInt(operation.values[0]))
                return stackRes
            }
            else {
                stackRes.push(parseInt(operation.values[0]))
                for (let index = 1; index < operation.values.length; index++) {
                    stackRes[0] = stackRes[0] - parseInt(operation.values[index])
                }
                return stackRes
            }
        case '+':
            if (stackRes.length > 0) {
                operation.values.unshift(stackRes[0])
                stackRes = []
            }

            if(operation.values.length < 1 ){
                throw new Error("Dear Mr. Alex, operation not allowed")
            }

            if (operation.values.length == 1) {
                stackRes.push(0 + parseInt(operation.values[0]))
                return stackRes
            }
            else {
                stackRes.push(parseInt(operation.values[0]))
                for (let index = 1; index < operation.values.length; index++) {
                    stackRes[0] = stackRes[0] + parseInt(operation.values[index])
                }
                return stackRes
            }
        case '/':
            if (stackRes.length > 0) {
                operation.values.unshift(stackRes[0])
                stackRes = []
            }

            if(operation.values.length < 1 ){
                throw new Error("Dear Mr. Alex, operation not allowed")
            }

            if (operation.values.length == 1) {
                if (parseInt(operation.values[0]) == 0) { throw new Error("Dear Mr. Alex, operation with divide by 0 is not allowed") }
                stackRes.push(0 / parseInt(operation.values[0]))
                return stackRes
            }
            else {
                stackRes.push(parseInt(operation.values[0]))
                for (let index = 1; index < operation.values.length; index++) {
                    if (parseInt(operation.values[index]) == 0) { throw new Error("Dear Mr. Alex, operation with divide by 0 is not allowed") }
                    stackRes[0] = stackRes[0] / parseInt(operation.values[index])
                }
                return stackRes
            }
        case '*':
            if (stackRes.length > 0) {
                operation.values.unshift(stackRes[0])
                stackRes = []
            }

            if(operation.values.length < 1 ){
                throw new Error("Dear Mr. Alex, operation not allowed")
            }

            if (operation.values.length == 1) {
                stackRes.push(0 * parseInt(operation.values[0]))
                return stackRes
            }
            else {
                stackRes.push(parseInt(operation.values[0]))
                for (let index = 1; index < operation.values.length; index++) {
                    stackRes[0] = stackRes[0] * parseInt(operation.values[index])
                }
                return stackRes
            }
        case 'sqrt':
            if (stackRes.length > 0) {
                throw new Error("Dear Mr. Alex, operation is not allowed")
            }

            if(operation.values.length < 1 ){
                throw new Error("Dear Mr. Alex, operation not allowed")
            }

            if (operation.values.length == 1) {
                if (parseInt(operation.values[0]) < 0 || parseInt(operation.values[0]) == 0 ) { throw new Error("Dear Mr. Alex, operation with divide by 0 is not allowed") }
                stackRes.push(Math.sqrt(parseInt(operation.values[0])))
                return stackRes
            }
            else {
                throw new Error("Dear Mr. Alex, operation is not allowed")
            }
        case 'max':
            if (stackRes.length > 0) {
                operation.values.unshift(stackRes[0])
                stackRes = []
            }

            if(operation.values.length < 1 ){
                throw new Error("Dear Mr. Alex, operation not allowed")
            }

            if (operation.values.length == 1) {
                stackRes.push(parseInt(operation.values[0]))
                return stackRes
            }
            else {
                operation.values = operation.values.map(el => { return parseInt(el) })
                return [Math.max(...operation.values)]
            }
    }
}

module.exports = {
    rpnCalculator,
    rpnCalculatorGoingFurther
}